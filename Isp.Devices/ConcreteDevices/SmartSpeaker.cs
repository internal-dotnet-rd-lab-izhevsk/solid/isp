﻿using System;
using System.IO;
using Isp.Devices.Interfaces;

namespace Isp.Devices.ConcreteDevices
{
	public class SmartSpeaker : ISmartDevice
	{
		public void RunApplication(string application)
		{
			throw new NotImplementedException();
		}

		public void TakePhoto()
		{
			throw new NotImplementedException();
		}

		public void StartVideoRecording()
		{
			throw new NotImplementedException();
		}

		public void StopVideoRecording()
		{
			throw new NotImplementedException();
		}

		public void Call(string number)
		{
			Console.WriteLine($"Calling number {number}");
		}

		public void SendMessage(string number, string message)
		{
			Console.WriteLine($"Send message '{message}' to {number}");
		}

		public void ExportData(Stream destination)
		{
			throw new NotImplementedException();
		}

		public void ImportData(Stream source)
		{
			Console.WriteLine("Import music");
		}
	}
}