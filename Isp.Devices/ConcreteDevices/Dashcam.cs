﻿using System;
using System.IO;
using Isp.Devices.Interfaces;

namespace Isp.Devices.ConcreteDevices
{
	/// <summary>
	/// Видеорегистратор
	/// </summary>
	public class Dashcam : ISmartDevice
	{
		public void RunApplication(string application)
		{
			throw new NotImplementedException();
		}

		public void TakePhoto()
		{
			Console.WriteLine("Take photo");
		}

		public void StartVideoRecording()
		{
			Console.WriteLine("Start recording video.");
		}

		public void StopVideoRecording()
		{
			Console.WriteLine("Stop recording video.");
		}

		public void Call(string number)
		{
			throw new NotImplementedException();
		}

		public void SendMessage(string number, string message)
		{
			throw new NotImplementedException();
		}

		public void ExportData(Stream destination)
		{
			Console.WriteLine("Export photos and videos");
		}

		public void ImportData(Stream source)
		{
			throw new NotImplementedException();
		}
	}
}