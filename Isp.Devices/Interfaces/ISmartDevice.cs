﻿using System.IO;

namespace Isp.Devices.Interfaces
{
	public interface ISmartDevice
	{
		public void RunApplication(string application);
		public void TakePhoto();
		public void StartVideoRecording();
		public void StopVideoRecording();
		public void Call(string number);
		public void SendMessage(string number, string message);
		public void ExportData(Stream destination);
		public void ImportData(Stream source);
	}
}